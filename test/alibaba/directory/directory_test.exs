defmodule Alibaba.DirectoryTest do
  use Alibaba.DataCase

  alias Alibaba.Directory

  describe "events" do
    alias Alibaba.Directory.Event

    @valid_attrs %{name: "some name", organisation: "some organisation", when: ~N[2010-04-17 14:00:00.000000]}
    @update_attrs %{name: "some updated name", organisation: "some updated organisation", when: ~N[2011-05-18 15:01:01.000000]}
    @invalid_attrs %{name: nil, organisation: nil, when: nil}

    def event_fixture(attrs \\ %{}) do
      {:ok, event} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Directory.create_event()

      event
    end

    test "list_events/0 returns all events" do
      event = event_fixture()
      assert Directory.list_events() == [event]
    end

    test "get_event!/1 returns the event with given id" do
      event = event_fixture()
      assert Directory.get_event!(event.id) == event
    end

    test "create_event/1 with valid data creates a event" do
      assert {:ok, %Event{} = event} = Directory.create_event(@valid_attrs)
      assert event.name == "some name"
      assert event.organisation == "some organisation"
      assert event.when == ~N[2010-04-17 14:00:00.000000]
    end

    test "create_event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Directory.create_event(@invalid_attrs)
    end

    test "update_event/2 with valid data updates the event" do
      event = event_fixture()
      assert {:ok, event} = Directory.update_event(event, @update_attrs)
      assert %Event{} = event
      assert event.name == "some updated name"
      assert event.organisation == "some updated organisation"
      assert event.when == ~N[2011-05-18 15:01:01.000000]
    end

    test "update_event/2 with invalid data returns error changeset" do
      event = event_fixture()
      assert {:error, %Ecto.Changeset{}} = Directory.update_event(event, @invalid_attrs)
      assert event == Directory.get_event!(event.id)
    end

    test "delete_event/1 deletes the event" do
      event = event_fixture()
      assert {:ok, %Event{}} = Directory.delete_event(event)
      assert_raise Ecto.NoResultsError, fn -> Directory.get_event!(event.id) end
    end

    test "change_event/1 returns a event changeset" do
      event = event_fixture()
      assert %Ecto.Changeset{} = Directory.change_event(event)
    end
  end

  describe "events" do
    alias Alibaba.Directory.Event

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def event_fixture(attrs \\ %{}) do
      {:ok, event} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Directory.create_event()

      event
    end

    test "list_events/0 returns all events" do
      event = event_fixture()
      assert Directory.list_events() == [event]
    end

    test "get_event!/1 returns the event with given id" do
      event = event_fixture()
      assert Directory.get_event!(event.id) == event
    end

    test "create_event/1 with valid data creates a event" do
      assert {:ok, %Event{} = event} = Directory.create_event(@valid_attrs)
    end

    test "create_event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Directory.create_event(@invalid_attrs)
    end

    test "update_event/2 with valid data updates the event" do
      event = event_fixture()
      assert {:ok, event} = Directory.update_event(event, @update_attrs)
      assert %Event{} = event
    end

    test "update_event/2 with invalid data returns error changeset" do
      event = event_fixture()
      assert {:error, %Ecto.Changeset{}} = Directory.update_event(event, @invalid_attrs)
      assert event == Directory.get_event!(event.id)
    end

    test "delete_event/1 deletes the event" do
      event = event_fixture()
      assert {:ok, %Event{}} = Directory.delete_event(event)
      assert_raise Ecto.NoResultsError, fn -> Directory.get_event!(event.id) end
    end

    test "change_event/1 returns a event changeset" do
      event = event_fixture()
      assert %Ecto.Changeset{} = Directory.change_event(event)
    end
  end

  describe "reurring_event" do
    alias Alibaba.Directory.Recurring_Event

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def recurring__event_fixture(attrs \\ %{}) do
      {:ok, recurring__event} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Directory.create_recurring__event()

      recurring__event
    end

    test "list_reurring_event/0 returns all reurring_event" do
      recurring__event = recurring__event_fixture()
      assert Directory.list_reurring_event() == [recurring__event]
    end

    test "get_recurring__event!/1 returns the recurring__event with given id" do
      recurring__event = recurring__event_fixture()
      assert Directory.get_recurring__event!(recurring__event.id) == recurring__event
    end

    test "create_recurring__event/1 with valid data creates a recurring__event" do
      assert {:ok, %Recurring_Event{} = recurring__event} = Directory.create_recurring__event(@valid_attrs)
      assert recurring__event.name == "some name"
    end

    test "create_recurring__event/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Directory.create_recurring__event(@invalid_attrs)
    end

    test "update_recurring__event/2 with valid data updates the recurring__event" do
      recurring__event = recurring__event_fixture()
      assert {:ok, recurring__event} = Directory.update_recurring__event(recurring__event, @update_attrs)
      assert %Recurring_Event{} = recurring__event
      assert recurring__event.name == "some updated name"
    end

    test "update_recurring__event/2 with invalid data returns error changeset" do
      recurring__event = recurring__event_fixture()
      assert {:error, %Ecto.Changeset{}} = Directory.update_recurring__event(recurring__event, @invalid_attrs)
      assert recurring__event == Directory.get_recurring__event!(recurring__event.id)
    end

    test "delete_recurring__event/1 deletes the recurring__event" do
      recurring__event = recurring__event_fixture()
      assert {:ok, %Recurring_Event{}} = Directory.delete_recurring__event(recurring__event)
      assert_raise Ecto.NoResultsError, fn -> Directory.get_recurring__event!(recurring__event.id) end
    end

    test "change_recurring__event/1 returns a recurring__event changeset" do
      recurring__event = recurring__event_fixture()
      assert %Ecto.Changeset{} = Directory.change_recurring__event(recurring__event)
    end
  end
end
