defmodule AlibabaWeb.Directory.EventView do
  use AlibabaWeb, :view
  alias AlibabaWeb.Directory.EventView

  def render("index.json", %{events: events}) do
    %{data: render_many(events, EventView, "event.json")}
  end

  def render("show.json", %{event: event}) do
    %{data: render_one(event, EventView, "event.json")}
  end

  def render("event.json", %{event: event}) do
    %{name: event.name,
    organisation: event.organisation,
    time_of: event.time_of,
    website: event.website
  }
  end
end
