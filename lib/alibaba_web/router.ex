defmodule AlibabaWeb.Router do
  use AlibabaWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Phauxth.Authenticate
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AlibabaWeb do
    pipe_through :browser
    get "/sitemap.xml", SitemapsController, :pages
    get "/blog", PageController, :blog
    get "/", PageController, :index
    get "/about", PageController, :about
    get "/contact", PageController, :contact
    get "/bikes", PageController, :bikes
    get "/steering", PageController, :steering
    get "/consult", PageController, :consult
    get "/activities", PageController, :activities
    resources "/posts", PostController
    get "/google93b50586b1832de5.html", PageController, :google
  end

  scope "/limlax2w3b3svt7m5g4a", AlibabaWeb do
    pipe_through :browser
    resources "/events", EventController
    resources "/sessions", SessionController, only: [:new, :create, :delete]
    resources "/recurring_event", Recurring_EventController
  end

  scope "/rktvcocdaki4m5o60sgb", AlibabaWeb do
    pipe_through :browser
    resources "/users", UserController
    resources "/sessions", SessionController, only: [:new, :create, :delete]
  end

  scope "/directory", AlibabaWeb.Directory, as: :api do
    pipe_through :api
    resources "/events", EventController, only: [:index]
  end

end
#limlax2w3b3svt7m5g4a
