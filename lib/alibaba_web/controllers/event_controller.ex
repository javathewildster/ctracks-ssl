defmodule AlibabaWeb.EventController do
  use AlibabaWeb, :controller
  import AlibabaWeb.Authorize
  alias Alibaba.Directory
  alias Alibaba.Directory.Event
  plug :user_check

  def index(conn, _params) do
    events = Directory.list_events()
    render conn, "index.html", events: events,
    layout: {AlibabaWeb.LayoutView, "event.html"}    
  end

  def new(conn, _params) do
    changeset = Directory.change_event(%Event{})
    render conn, "new.html", changeset: changeset,
    layout: {AlibabaWeb.LayoutView, "event.html"}
  end

  def create(conn, %{"event" => event_params}) do
    case Directory.create_event(event_params) do
      {:ok, event} ->
        conn
        |> put_flash(:info, "Event created successfully.")
        |> redirect(to: event_path(conn, :show, event))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    event = Directory.get_event!(id)
    render conn, "show.html", event: event,
    layout: {AlibabaWeb.LayoutView, "event.html"}
  end

  def edit(conn, %{"id" => id}) do
    event = Directory.get_event!(id)
    changeset = Directory.change_event(event)
    render conn, "edit.html", event: event, changeset: changeset,
    layout: {AlibabaWeb.LayoutView, "event.html"}
  end

  def update(conn, %{"id" => id, "event" => event_params}) do
    event = Directory.get_event!(id)

    case Directory.update_event(event, event_params) do
      {:ok, event} ->
        conn
        |> put_flash(:info, "Event updated successfully.")
        |> redirect(to: event_path(conn, :show, event))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", event: event, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    event = Directory.get_event!(id)
    {:ok, _event} = Directory.delete_event(event)

    conn
    |> put_flash(:info, "Event deleted successfully.")
    |> redirect(to: event_path(conn, :index))
  end
end
