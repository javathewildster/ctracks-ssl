defmodule AlibabaWeb.SitemapsController do
  use AlibabaWeb, :controller
  use Plugmap

  defsitemap :pages do
    static do
      page "https://communitytracks.net", changefreq: "daily", priority: 1.0
      page "https://communitytracks.net/about", changefreq: "daily", priority: 0.5
      page "https://communitytracks.net/blog", changefreq: "daily", priority: 1.0
    end
  end


end
