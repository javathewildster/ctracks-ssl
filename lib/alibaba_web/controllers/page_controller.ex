defmodule AlibabaWeb.PageController do
  use AlibabaWeb, :controller
  alias Alibaba.Blog
  alias Alibaba.Blog.Post

  def index(conn, _params) do
    posts = Blog.list_posts_frontpage()
    render conn, "index.html", posts: posts,
    layout: {AlibabaWeb.LayoutView, "home.html"}
  end

  def about(conn, _params) do
    render conn, "about.html"
  end

  def contact(conn, _params) do
    render conn, "contact.html"

  end

  def bikes(conn, _params) do
    render conn, "bikes.html"
  end

  def steering(conn, _params) do
    render conn, "steering.html"
  end

  def consult(conn, _params) do
    render conn, "consult.html"
  end

  def activities(conn, _params) do
    render conn, "activities.html",
    layout: {AlibabaWeb.LayoutView, "activities.html"}
  end

  def blog(conn, _params) do
    posts = Blog.list_posts()
    render conn, "posts.html", posts: posts
  end

  def google(conn, _params) do
    render conn, "google.html",
    layout: {AlibabaWeb.LayoutView, "blank.html"}
  end

end
