defmodule AlibabaWeb.Directory.EventController do
  use AlibabaWeb, :controller

  alias Alibaba.Directory
  alias Alibaba.Directory.Event

  action_fallback AlibabaWeb.FallbackController

  def index(conn, _params) do
    events = Directory.list_events()
    render(conn, "index.json", events: events)
  end

end
