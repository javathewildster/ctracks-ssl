defmodule Alibaba.Directory.Event do
  use Ecto.Schema
  import Ecto.Changeset
  alias Alibaba.Directory.Event


  schema "events" do
    field(:name, :string)
    field(:organisation, :string)
    field(:description, :string)
    field(:time_of, :naive_datetime)
    field(:contact, :string)
    field(:email, :string)
    field(:phone, :string)
    field(:website, :string)
    timestamps()
  end

  @doc false
  def changeset(%Event{} = event, attrs) do
    event
    |> cast(attrs, [:name, :organisation, :time_of, :website])
    |> validate_required([:name, :organisation, :time_of, :website])
  end
end
