defmodule Alibaba.Blog.Post do
  use Ecto.Schema
  import Ecto.Changeset
  use Arc.Ecto.Schema

  schema "posts" do
    field :body, :string
    field :image, Alibaba.Image.Type
    field :title, :string
    field :time_of, :naive_datetime
    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:title, :body, :time_of])
    |> cast_attachments(attrs, [:image])
    |> validate_required([:title, :body, :image, :time_of])
  end
end
