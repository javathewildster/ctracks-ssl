# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :alibaba,
  ecto_repos: [Alibaba.Repo]

# Configures the endpoint
config :alibaba, AlibabaWeb.Endpoint,
  url: [host: nil],
  secret_key_base: "cgA+N4/QuOCiUgOBcfSur2fRdNa2MGKjE83hWBnK5Ze3g4ay9sjeKJxwEy2+CKlW",
  render_errors: [view: AlibabaWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Alibaba.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Phauxth authentication configuration
config :phauxth,
  token_salt: "wL5mfih9",
  endpoint: AlibabaWeb.Endpoint

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]



config :arc,
   storage: Arc.Storage.Local
# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
