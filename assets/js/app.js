// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"
import Vue from 'vue'
import 'vueify/lib/insert-css'
import Frame from './components/map/Frame.vue'
import TheHeader from './components/shared/TheHeader.vue'
import TheFooter from './components/shared/TheFooter.vue'
import Contact from './components/Contact.vue'
import GoogleMapsLoader from 'google-maps'
import Buefy from 'buefy'
import Activities from './components/FrontActivities.vue'
import Calendar from './components/Calendar.vue'

Vue.use(Buefy)
Vue.use(require('vue-moment'));
var Promise = require('es6-promise').Promise;


new Vue({
  el: '#app',
  template: '<Frame/>',
  components: { Frame }
})
new Vue({
  el: '#header',
  template: '<TheHeader/>',
  components: { TheHeader }
})
new Vue({
  el: '#footer',
  template: '<TheFooter/>',
  components: { TheFooter }
})
new Vue({
  el: '#contact',
  template: '<Contact/>',
  components: { Contact }
})
new Vue({
  el: '#activities',
  template: '<Activities/>',
  components: { Activities }
})
new Vue({
  el: '#calendar',
  template: '<Calendar/>',
  components: { Calendar }

})

document.addEventListener('DOMContentLoaded', function () {
  
    // Get all "navbar-burger" elements
    var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
  
    // Check if there are any nav burgers
    if ($navbarBurgers.length > 0) {
  
      // Add a click event on each of them
      $navbarBurgers.forEach(function ($el) {
        $el.addEventListener('click', function () {
  
          // Get the target from the "data-target" attribute
          var target = $el.dataset.target;
          var $target = document.getElementById(target);
  
          // Toggle the class on both the "navbar-burger" and the "navbar-menu"
          $el.classList.toggle('is-active');
          $target.classList.toggle('is-active');
  
        });
      });
    }
  
  });
  