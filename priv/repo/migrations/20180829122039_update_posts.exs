defmodule Alibaba.Repo.Migrations.UpdatePosts do
  use Ecto.Migration

  def change do
    alter table(:posts) do
      add :time_of, :naive_datetime
    end
  end
end
