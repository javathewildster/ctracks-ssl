defmodule Alibaba.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :name, :string
      add :organisation, :string
      add :description, :string
      add :time_of, :naive_datetime
      add :contact, :string
      add :email, :string
      add :phone, :string
      add :website, :string
      timestamps()
    end

  end
end
